#!/usr/bin/env python
import os, sys

import pika


RABBITMQ_HOST = os.environ.get("RABBITMQ_HOST", "localhost")
RABBITMQ_MESSAGE = os.environ.get("RABBITMQ_MESSAGE", "Hello World!")


# establish a connection
connection = pika.BlockingConnection(pika.ConnectionParameters(RABBITMQ_HOST))
channel = connection.channel()

def close_the_server(func):
    try:
        func()
    except KeyboardInterrupt:
        print("Interrupted!")
        try:
            sys.exit(0)
        except SystemError:
            os._exit(0)
