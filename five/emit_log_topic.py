import sys

from channel import channel, connection, RABBITMQ_MESSAGE


RABBITMQ_EXCHANGE = "topic_logs"
EXCHANGE_TYPE = "topic"


channel.exchange_declare(exchange=RABBITMQ_EXCHANGE, exchange_type=EXCHANGE_TYPE)

routing_key = sys.argv[1] if len(sys.argv) > 2 else "anonymous.info"
message = " ".join(sys.argv[1:]) or f"message: {RABBITMQ_MESSAGE}"
channel.basic_publish(exchange=RABBITMQ_EXCHANGE, routing_key=routing_key, body=message)
print(f" [x] Sent {routing_key}:{message}")
connection.close()
