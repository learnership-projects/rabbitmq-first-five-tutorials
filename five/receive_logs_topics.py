import sys

from channel import channel, close_the_server


RABBITMQ_QUEUE = ""
RABBITMQ_EXCHANGE = "topic_logs"
EXCHANGE_TYPE = "topic"


channel.exchange_declare(exchange=RABBITMQ_EXCHANGE, exchange_type=EXCHANGE_TYPE)

result = channel.queue_declare(queue=RABBITMQ_QUEUE, exclusive=True)
queue_name = result.method.queue

binding_keys = sys.argv[1:]

if not binding_keys:
    sys.stderr.write("Usage: %s [binding_key]...\n" % sys.arg[0])
    sys.exit(1)

for binding_key in binding_keys:
    channel.queue_bind(
        exchange=RABBITMQ_EXCHANGE, queue=queue_name, routing_key=binding_key
    )


def get_the_message():
    def callback(ch, method, properties, body):
        print(f" [x] Received {method.routing_key}{body}")

    print(" [*] Waiting for logs. To exit press CTRL+C")

    channel.basic_consume(queue=queue_name, on_message_callback=callback, auto_ack=True)

    channel.start_consuming()


if __name__ == "__main__":
    close_the_server(get_the_message)
