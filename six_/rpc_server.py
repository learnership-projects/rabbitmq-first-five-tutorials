import pika

from channel import channel, close_the_server


RABBITMQ_QUEUE = "rpc_queue"
RABBITMQ_EXCHANGE = ""

channel.queue_declare(queue=RABBITMQ_QUEUE)


def fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fib(n - 1) + fib(n - 2)


def send_the_response():
    def on_request(ch, method, props, body):
        n = int(body)

        print(f" [.] fib({n})")
        response = fib(n)

        ch.basic_publish(
            exchange=RABBITMQ_EXCHANGE,
            routing_key=props.reply_to,
            properties=pika.BasicProperties(correlation_id=props.correlation_id),
            body=str(response),
        )
        ch.basic_ack(delivery_tag=method.delivery_tag)

    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(queue=RABBITMQ_QUEUE, on_message_callback=on_request)

    print(" [x] Awaiting RPC requests")
    channel.start_consuming()


if __name__ == "__main__":
    close_the_server(send_the_response)
