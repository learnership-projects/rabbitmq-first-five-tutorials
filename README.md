# RabbitMQ

This project is the first six official tutorials application from here: https://www.rabbitmq.com/getstarted.html.
# How to run this project

## Install the package and dependencies, and run the image from the docker
1. **Create a virtual environment and activate it:**

For Windows
```bash shell
python -m venv <name_of_your_virtual_env>
```

For Linux/macOS
```Ubuntu shell
pipenv install
```
2. **Activate your virtual environment:**

For Windows
```bash shell
.\<name_of_your_virtual_env>\Scripts\activate
```

For Linux/macOS
```Ubuntu shell
pipenv shell
```

3. **Install the package:**

For Linux/macOS/Windows
```shell
python setup.py develop
```

4. **Install the dependencies:**

For Linux/macOS
```Ubuntu shell
pipenv install -r requirements.txt
```
For Windows
```bash
pip install -r requirements.txt
```

5. **Run the Rabbitmq image within your docker:**
```
docker compose up -d
```

## Run the project's tutorials:
- Add your host connection and message to the broker, e.g. create a shell script:
```
#\!/bin/sh

export RABBITMQ_HOST=localhost
export RABBITMQ_MESSAGE="Hello World!"
```

for permissions: 
```bash shell
chmod +x your_script.sh
```
And run it:
```bash shell
./your_script.sh
```

1. **first tutorial.**
- Make sure you've added the host connection and message to the broker as env vars using a shell script.

- Run your producer script:
```
python one/send.py
# [x] Sent 'Hello World!'`
```

- Run your consumer script:
```
python one/receive.py
# [*] Waiting for messages. To exit press CTRL+C
# [x] Received b'Hello World!'
```
Press CTRL+C to exit.

2. **second tutorial**
- Make sure you've added the host connection and message to the broker as env vars using a shell script.

- Run your consumer scripts

first console:
```
python two/worker.py

# [*] Waiting for messages. To exit press CTRL+C
```

second console:
```
python two/worker.py
# [*] Waiting for messages. To exit press CTRL+C
```

- Run your producer script:
third console
```
python two/new_task.py

# [x] Sent Hello World!
```

Look at your first console:
```
# [*] Waiting for messages. To exit press CTRL+C
# [x] Received Hello World!
# [x] Done
```

- Change your message to the broker in the shell, don't forget to run it.

For permissions: 
```bash shell
chmod +x your_script.sh
```
And run it:
```bash shell
./your_script.sh
```

- Run your producer script again

third console:
```
python two/new_task.py

# [x] Sent Second Hello World!
```
Look at your Second console:
```
# [*] Waiting for messages. To exit press CTRL+C
# [x] Received Second Hello World!
# [x] Done
```
The tasks were distributed evenly between these two consumers - c1 and c2.
Press `CTRL+C` to exit in the first and second consoles.

3. **Third tutorial**
- Make sure you've added the host connection and message to the broker as env vars using a shell script.

- Run two or more consumers in different consoles, and both of them will receive any log messages from the Producer.
```
python three/receive_logs.py
# [*] Waiting for logs. To exit press CTRL+C
```

You can save logs to a file, open another console
```
python three/receive_logs.py > logs_from_rabbit.log
```

In another console, run your Producer
```
python three/emit_log.py
# [x] Sent info: Hello World!
```
In other consoles, the Consumers share log messages.
```
#  [*] Waiting for logs. To exit press CTRL+C
# [x] Received b'info: Second Hello World!'
```

4. **Fourth tutorial**
- Make sure you've added the host connection and message to the broker as env vars using a shell script. 

- In this tutorial severity can be either one of the three 'info', 'warning' and 'error'.

If you want to send only 'info' log messages to one c1 in your console:
```
python four/receive_logs_direct.py info
# => [*] Waiting for logs. To exit press CTRL+C
```

Or if you want to save only 'warning' and 'error' log messages to your file, in another console:
```
python four/receive_logs_direct.py warning error > logs_from_rabbit.log
```

Or If you'd like to see all the log messages on your screen, open yet another console and do:
```
python four/receive_logs_direct.py info warning error
# => [*] Waiting for logs. To exit press CTRL+C
```

Now run the Producer script to emit only the 'error' log messages and see what's gonna happen.
```
python four/emit_log_direct.py error "Run. Run. Or it will explode."
# => [x] Sent 'error':'Run. Run. Or it will explode.'
```
Or emit only the 'info' log messages
```
python four/emit_log_direct.py info "Hello World!"
 [x] Sent info:info Hello World!
```
Different consumers received log messages from the exchanges whose logs have subscribed to.

5. **fifth tutorial**
- Make sure you've added the host connection and message to the broker as env vars using a shell script. 

To receive all the log messages use the string '#' binding key in the first console,
```
python five/receive_logs_topics.py "#"
#  [*] Waiting for logs. To exit press CTRL+C
```

To receive all logs from the facility "kern" in another console:
```
python five/receive_logs_topics.py "kern.*"
# [*] Waiting for logs. To exit press CTRL+C
```

Or if we want to hear only about "critical" logs in yet another console:
```
python five/receive_logs_topics.py "*.critical"
# [*] Waiting for logs. To exit press CTRL+C
```
Or we can create multiple bindings in the fourth console:
```
python five/receive_logs_topics.py "kern.*" "*.critical"
# [*] Waiting for logs. To exit press CTRL+C
```

Now let's run the producer's script to emit a log with a routing key "kern.critical" type in the fifth console:
```
python five/emit_log_topic.py "kern.critical" "A critical kernel error"
# [x] Sent kern.critical:kern.critical A critical kernel error
```
Now if we look at the previous four consoles all of the consumers 'c1-c4' have received the log message: 
"[x] Received kern.criticalb'kern.critical A critical kernel error'"

6. **Sixth tutorial**

- We can start the server by running the following:
```
 python six_/rpc_server.py
 # '[x] Awaiting RPC requests'
```

- To request a Fibonacci number run the client:
```
python six_/rpc_client.py
# '[x] Requesting fib(30)'
# '[.] Got 832040'
```
