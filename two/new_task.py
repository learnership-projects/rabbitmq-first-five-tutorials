import sys

import pika

from channel import RABBITMQ_MESSAGE, channel, connection

RABBITMQ_QUEUE = "task_queue"
RABBITMQ_EXCHANGE = ""


channel.queue_declare(queue=RABBITMQ_QUEUE, durable=True)

message = " ".join(sys.argv[1:]) or RABBITMQ_MESSAGE
channel.basic_publish(
    exchange=RABBITMQ_EXCHANGE,
    routing_key=RABBITMQ_QUEUE,
    body=message,
    properties=pika.BasicProperties(delivery_mode=pika.DeliveryMode.Persistent),
)
print(f" [x] Sent {message}")

connection.close()
