import time

from channel import channel, close_the_server


RABBITMQ_QUEUE = "task_queue"

channel.queue_declare(queue=RABBITMQ_QUEUE, durable=True)


def get_the_message():
    def callback(ch, method, properties, body):
        print(f" [x] Received {body.decode()}")

        time.sleep(body.count(b"."))

        print(" [x] Done")

        ch.basic_ack(delivery_tag=method.delivery_tag)

    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(queue=RABBITMQ_QUEUE, on_message_callback=callback)

    print(" [*] Waiting for messages. To exit press CTRL+C")
    channel.start_consuming()


if __name__ == "__main__":
    close_the_server(get_the_message)
