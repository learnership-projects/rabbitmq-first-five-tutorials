from setuptools import setup, find_packages

setup(
    name="rabbitmq_project",
    author="Kholofelo",
    author_email="rkmoropane@gmail.com",
    description="RabbitMQ Tutorials on 'https://www.rabbitmq.com/getstarted.html'",
    long_description=open("README.md").read(),
    packages=find_packages(include=["one", "two", "three", "four", "five", "six_"]),
)
