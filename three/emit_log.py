import sys

from channel import channel, connection, RABBITMQ_MESSAGE

RABBITMQ_QUEUE = ""
RABBITMQ_EXCHANGE = "logs"
EXCHANGE_TYPE = "fanout"


channel.exchange_declare(exchange=RABBITMQ_EXCHANGE, exchange_type=EXCHANGE_TYPE)

message = " ".join(sys.argv[1:]) or f"info: {RABBITMQ_MESSAGE}"
channel.basic_publish(
    exchange=RABBITMQ_EXCHANGE, routing_key=RABBITMQ_QUEUE, body=message
)
print(f" [x] Sent {message}")
connection.close()
