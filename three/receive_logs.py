from channel import channel, close_the_server

RABBITMQ_QUEUE = ""
RABBITMQ_EXCHANGE = "logs"
EXCHANGE_TYPE = "fanout"

channel.exchange_declare(exchange=RABBITMQ_EXCHANGE, exchange_type=EXCHANGE_TYPE)

result = channel.queue_declare(queue=RABBITMQ_QUEUE, exclusive=True)
queue_name = result.method.queue

channel.queue_bind(exchange=RABBITMQ_EXCHANGE, queue=queue_name)


def get_the_message():
    def callback(ch, method, properties, body):
        print(f" [x] Received {body}")

    print(" [*] Waiting for logs. To exit press CTRL+C")

    channel.basic_consume(queue=queue_name, on_message_callback=callback, auto_ack=True)

    channel.start_consuming()


if __name__ == "__main__":
    close_the_server(get_the_message)
