import sys

from channel import channel, connection, RABBITMQ_MESSAGE


RABBITMQ_EXCHANGE = "direct_logs"
EXCHANGE_TYPE = "direct"


channel.exchange_declare(exchange=RABBITMQ_EXCHANGE, exchange_type=EXCHANGE_TYPE)

severity = sys.argv[1] if len(sys.argv) > 1 else "info"
message = " ".join(sys.argv[1:]) or f"message: {RABBITMQ_MESSAGE}"
channel.basic_publish(exchange=RABBITMQ_EXCHANGE, routing_key=severity, body=message)
print(f" [x] Sent {severity}:{message}")
connection.close()
