from channel import (
    channel,
    connection,
    RABBITMQ_MESSAGE,
)

RABBITMQ_EXCHANGE = ""
RABBITMQ_QUEUE = "hello"
#  create a 'hello' queue to which the message will be delivered to
channel.queue_declare(queue=RABBITMQ_QUEUE)

# An Exchange to which a message can go through to the queue
channel.basic_publish(
    exchange=RABBITMQ_EXCHANGE, routing_key=RABBITMQ_QUEUE, body=RABBITMQ_MESSAGE
)
print(f" [x] Sent '{RABBITMQ_MESSAGE}'")

connection.close()
