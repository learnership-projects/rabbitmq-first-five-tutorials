from channel import channel, close_the_server

RABBITMQ_QUEUE = "hello"

#  create a 'hello' queue to which the receiver fetch the message from
channel.queue_declare(queue=RABBITMQ_QUEUE)


def get_the_message():
    def callback(ch, method, properties, body):
        print(f" [x] Received {body}")

    channel.basic_consume(
        queue=RABBITMQ_QUEUE, on_message_callback=callback, auto_ack=True
    )

    print(" [*] Waiting for messages. To exit press CTRL+C")
    channel.start_consuming()


if __name__ == "__main__":
    close_the_server(get_the_message)
